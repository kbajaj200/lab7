/*  Kunal Bajaj
    CPSC 1021 Section 002
    Kbajaj@clemson.edu
    Nushrat Humaira
    Evan Hastings
*/

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include <cstdlib>
#include <time.h>

using namespace std;

typedef struct Employee{
    string lastName;
    string firstName;
    int birthYear;
    double hourlyWage;
}employee;

bool name_order(const employee& lhs, const employee& rhs);
int myrandom (int i) { return rand()%i;}

int main(int argc, char const *argv[]) {  
    // IMPLEMENT as instructed below  
    /*This is to seed the random generator */  
    srand(unsigned (time(0)));

/*Create an array of 10 employees and fill information from standard input with prompt messages*/
Employee employees[10];
for(int i = 0; i < 10; i++)
{
    cout << "What is your last name?";
    cin >> employees[i].lastName;
    cout << "What is your first name?";
    cin >> employees[i].firstName;
    cout << "What is your Birthyear?";
    cin >> employees[i].birthYear;
    cout << "What is your Hourly Wage?";
    cin >> employees[i].hourlyWage;
    cout << endl;
}

/*After the array is created and initialzed we call random_shuffle() see the   
 * *notes to determine the parameters to pass in.*/
// takes list and shuffles them.
// pointer to begnning of array and one past the end and also myrandom function   
   random_shuffle(employees,employees+10,myrandom);

/*Build a smaller array of 5 employees from the first five cards of the array created    
  * *above*/
Employee list[5];
for(int i = 0; i < 5; i++)
{
    list[i] = employees[i];
}

 /*Sort the new array.  Links to how to call this function is in the specs     
   * *provided*/
// Sorts the list according to the name order
sort(list, list+5,&name_order);

 /*Now print the array below */
// Uses a range based for loop to print out the list array
// Uses setw to format the list
for(auto i : list)
{
   cout << setw(i.lastName.length()+5) <<i.lastName << "," << i.firstName << endl;
   cout << setw(9) << i.birthYear << endl;
   cout << setw(10) << i.hourlyWage << endl;
}
   return 0;
}

/*This function will be passed to the sort funtion. Hints on how to implement* 
 * this is in the specifications document.*/
// Takes the last names and sorts them by name left side to right side
 bool name_order(const employee& lhs, const employee& rhs) {  
     // IMPLEMENT
     return lhs.lastName < rhs.lastName;
}













